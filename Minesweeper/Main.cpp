#include <fstream>

#include "GameWindow.h"
#include "Constants.h"

#include "../Logging/Logging.h"

int main()
{
	Logger::Log(std::cout, Logger::Level::INFO, "Opened Minesweeper...");
	GameWindow game{ MineField(Constants::kBeginnerWidth,Constants::kBeginnerHeight ,Constants::kBeginnerMines) };
	game.Run();
	Logger::Log(std::cout, Logger::Level::INFO, "Ended Minesweeper...");

	return 0;
}
#pragma once

#include <memory>

#include "BasicWindow.h"
#include "Button.h"
#include "MineField.h"
#include "Constants.h"
#include "Resource.h"

class MenuWindow : public BasicWindow
{
public:
	MenuWindow();
	MenuWindow(const MenuWindow& menuWindow);
	MenuWindow(MenuWindow&& menuWindow) noexcept;
	MenuWindow& operator=(const MenuWindow& menuWindow);
	MenuWindow& operator=(MenuWindow&& menuWindow) noexcept;
	bool Run(MineField& mineField);
	~MenuWindow();

	const sf::Text GetBeginnerText()const;
	const sf::Text GetIntermediateText()const;
	const sf::Text GetExpertText()const;
	const sf::Text GetCustomText()const;
	const sf::Text GetDimensionsText()const;

	const Button GetHeightCustom()const;
	const Button GetWidthCustom()const;
	const Button GetMinesCustom()const;

private:
	enum class Difficulty :uint8_t {
		None,
		Beginner,
		Intermediate,
		Expert,
		Custom
	}m_difficulty : 3;

	enum class CustomDimensions :uint8_t {
		None,
		Height,
		Width,
		Mines
	}m_customDimensions : 2;

	Button m_beginnerButton{ "    ",{10,50} };
	Button m_intermediateButton{ "    ",{10,100} };
	Button m_expertButton{ "    ",{10,150} };
	Button m_customButton{ "    ",{10,200} };

	sf::Text m_beginnerText{ "Beginner           9           9            10",Resource::arial,20 };
	sf::Text m_intermediateText{ "Intermediate     16         16           40",Resource::arial,20 };
	sf::Text m_expertText{ "Expert               16         30           99",Resource::arial,20 };
	sf::Text m_customText{ "Custom",Resource::arial,20 };
	sf::Text m_dimensionsText{ "Height   Width    Mines",Resource::arial,20 };

	Button m_heightCustom{ "",{ 200,200 }, {50,26} };
	Button m_widthCustom{ "",{ 280,200 }, {50,26} };
	Button m_minesCustom{ "",{ 365,200}, {50,26} };
	Button m_newGame{ "  New Game  ",{10,250} };

	void Decolor(const Difficulty difficulty);
	void DecolorCustomDimensions();
	bool ExecuteEvents(MineField& mineField, bool& changedMinefield);
	void Draw();
};


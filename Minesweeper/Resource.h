#pragma once

#include "SFML/Graphics.hpp"

namespace Resource
{
	extern sf::Font arial;
	extern sf::Texture tilesTexture;
	extern sf::Texture bordersTexture;
	extern sf::Texture digitsTexture;
	extern sf::Texture facesTexture;
	extern sf::Texture rulesTexture;
}
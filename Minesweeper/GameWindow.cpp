#include "GameWindow.h"

sf::Font Resource::arial;
sf::Texture Resource::tilesTexture;
sf::Texture Resource::bordersTexture;
sf::Texture Resource::digitsTexture;
sf::Texture Resource::facesTexture;
sf::Texture Resource::rulesTexture;

void GameWindow::Load() const
{
	if (!Resource::arial.loadFromFile("resources/fonts/ArialCEBold.ttf"))
		throw "Font not loaded!";
	if (!Resource::tilesTexture.loadFromFile("resources/textures/tiles.png"))
		throw "Tiles not loaded!";
	if (!Resource::bordersTexture.loadFromFile("resources/textures/borders.png"))
		throw "Borders not loaded!";
	if (!Resource::digitsTexture.loadFromFile("resources/textures/digits.png"))
		throw "Digits not loaded!";
	if (!Resource::facesTexture.loadFromFile("resources/textures/faces.png"))
		throw "Faces not loaded!";
	if (!Resource::rulesTexture.loadFromFile("resources/textures/rulez.png"))
		throw "Tiles not loaded!";
}

GameWindow::GameWindow()
	:GameWindow{ MineField(0,0,0) }
{
}

GameWindow::GameWindow(MineField game)
	: m_gameField{ game },
	BasicWindow(sf::VideoMode(game.GetWidth()* Constants::kTileLenght + 2 * Constants::kBorderLenght,
		game.GetHeight()* Constants::kTileLenght + 3 * Constants::kBorderLenght + 3 * Constants::kTileLenght),
		"Minesweeper", sf::Style::Close)
{
	SetVisibile(true);

	Load();
	m_menuButton = Button{ "  Menu  ",{3,3} };
	m_helpButton = Button{ "  Help  ",{m_menuButton.getPositon().x + m_menuButton.getSize().x + 5,3} };
	m_hintButton = Button{ "  Hint  ",{m_helpButton.getPositon().x + m_helpButton.getSize().x + 5,3} };

}

GameWindow::GameWindow(const GameWindow& other)
{
	*this = other;
}

GameWindow::GameWindow(GameWindow&& other) noexcept
{
	*this = std::move(other);
}

GameWindow& GameWindow::operator=(const GameWindow& other)
{
	if (this != &other)
	{
		m_gameField = other.m_gameField;
		m_menuButton = other.m_menuButton;
		m_helpButton = other.m_helpButton;
		m_hintButton = other.m_hintButton;
		m_menuWindow = other.m_menuWindow;
		m_helpWindow = other.m_helpWindow;
	}
	return *this;
}

GameWindow& GameWindow::operator=(GameWindow&& other) noexcept
{
	if (this != &other)
	{
		m_gameField = other.m_gameField;
		m_menuButton = other.m_menuButton;
		m_helpButton = other.m_helpButton;
		m_hintButton = other.m_hintButton;
		m_menuWindow = other.m_menuWindow;
		m_helpWindow = other.m_helpWindow;
		new(&other) GameWindow();
	}
	return *this;
}

GameWindow::~GameWindow()
{
}

void GameWindow::Draw(uint16_t& time, Faces& faceState)
{
	static sf::Sprite tiles(Resource::tilesTexture);
	static sf::Sprite borders(Resource::bordersTexture);
	static sf::Sprite digits(Resource::digitsTexture);
	static sf::Sprite faces(Resource::facesTexture);

	MineField::Position tilePosition;
	static uint16_t auxValue;

	borders.setRotation(0);

	auto drawCorner = [&borders=borders,&m_window=m_window](uint16_t xPos, uint16_t yPos, uint8_t borderPos) -> void
	{
		borders.setTextureRect(sf::IntRect(borderPos * Constants::kBorderLenght, 0,
			Constants::kBorderLenght, Constants::kBorderLenght));
		borders.setPosition(xPos, yPos);
		m_window->draw(borders);
	};
	auto drawDigit = [&digits=digits,&m_window = m_window](uint16_t xPos, uint16_t yPos, uint8_t digitPos) -> void
	{
		digits.setTextureRect(sf::IntRect(digitPos * Constants::kDigitLenght, 0,
			Constants::kDigitLenght, Constants::kDigitHeight));
		digits.setPosition(xPos, yPos);
		m_window->draw(digits);
	};
	auto drawSide = [&borders=borders,&m_window = m_window](uint16_t xPos, uint16_t yPos) -> void
	{
		borders.setPosition(xPos, yPos);
		m_window->draw(borders);
	};
	auto drawFace = [&faces=faces,&m_window = m_window](uint16_t xPos, uint16_t yPos, Faces selectedFace) -> void
	{
		faces.setTextureRect(sf::IntRect(static_cast<uint8_t> (selectedFace) * Constants::kFaceLenght,
			0, Constants::kFaceLenght, Constants::kFaceLenght));
		faces.setPosition(xPos, yPos);
		m_window->draw(faces);
	};

	drawCorner(0, Constants::kTileLenght, 0); //top left corner
	drawCorner(0, m_window->getSize().y - Constants::kBorderLenght, 2); //bottom left corner
	drawCorner(0, 2 * Constants::kTileLenght + Constants::kBorderLenght + Constants::kTileLenght, 4); //middle left corner
	drawCorner(m_window->getSize().x - Constants::kBorderLenght, 3 * Constants::kTileLenght + Constants::kBorderLenght, 5); //middle right corner
	drawCorner(m_window->getSize().x - Constants::kBorderLenght, Constants::kTileLenght, 1); //top right corner
	drawCorner(m_window->getSize().x - Constants::kBorderLenght, m_window->getSize().y - Constants::kBorderLenght, 3); //bottom right corner

	borders.setTextureRect(sf::IntRect(6 * Constants::kBorderLenght, 0, Constants::kTileLenght, Constants::kBorderLenght));
	for (uint16_t index = 0; index < m_gameField.GetWidth(); ++index)
	{
		auxValue = index * Constants::kTileLenght + Constants::kBorderLenght;
		drawSide(auxValue, Constants::kTileLenght); //top border
		drawSide(auxValue, 3 * Constants::kTileLenght + Constants::kBorderLenght); //middle border
		drawSide(auxValue, m_window->getSize().y - Constants::kBorderLenght); //bottom border
	}

	borders.setRotation(270);

	for (uint8_t index = 0; index < 2; ++index)
	{
		auxValue = index * Constants::kTileLenght + Constants::kBorderLenght + 2 * Constants::kTileLenght;
		borders.setTextureRect(sf::IntRect(6 * Constants::kBorderLenght, 0, Constants::kTileLenght, Constants::kBorderLenght));
		drawSide(0, auxValue); //left
		drawSide(m_window->getSize().x - Constants::kBorderLenght, auxValue); //right
	}
	for (uint16_t index1 = 0; index1 < m_gameField.GetHeight(); ++index1)
	{
		borders.setTextureRect(sf::IntRect(6 * Constants::kBorderLenght, 0, Constants::kTileLenght, Constants::kBorderLenght));
		auxValue = index1 * Constants::kTileLenght + 2 * Constants::kBorderLenght + 4 * Constants::kTileLenght;

		drawSide(0, auxValue); //left

		for (uint16_t index2 = 0; index2 < m_gameField.GetWidth(); ++index2)
		{
			tilePosition = { index1,index2 };
			tiles.setTextureRect(sf::IntRect
			(selectTile(m_gameField[tilePosition]) * Constants::kTileLenght,
				0, Constants::kTileLenght, Constants::kTileLenght));
			tiles.setPosition((float)index2 * Constants::kTileLenght + Constants::kBorderLenght,
				(float)index1 * Constants::kTileLenght + 2 * Constants::kBorderLenght + 3 * Constants::kTileLenght);
			m_window->draw(tiles);
		}
		drawSide(m_window->getSize().x - Constants::kBorderLenght, auxValue); //right
	}

	m_window->draw(m_menuButton);
	m_window->draw(m_helpButton);
	m_window->draw(m_hintButton);

	drawTimer(time, drawDigit);
	drawFlagCounter(m_gameField.GetNumberBombs() - m_gameField.GetNumberFlags(), drawDigit);
	drawFace((m_window->getSize().x - Constants::kFaceLenght) / 2,
		Constants::kBorderLenght + 2 * Constants::kTileLenght - Constants::kFaceLenght / 2, faceState);

	m_window->display();
	m_window->clear(sf::Color(192, 192, 192));
}

void GameWindow::ExecuteEvents(uint16_t& time, Faces& faceState)
{
	static std::chrono::time_point<std::chrono::steady_clock> start;
	static bool firstClick = true;
	static bool faceWasPressed = false;
	static bool tileWasPressed = false;

	sf::Vector2i mousePosition = sf::Mouse::getPosition(*m_window);
	uint16_t xMouse;
	uint16_t yMouse;
	uint16_t xAux;
	uint16_t yAux;
	static MineField::Position previousTileIndex = { UINT16_MAX,UINT16_MAX };
	MineField::Position tilePosition;

	sf::Event event;

	bool menuOpen = false;
	bool helpOpen = false;

	while (m_window->pollEvent(event))
	{
		if (event.type == sf::Event::Closed)
		{
			m_window->close();
			if (m_menuWindow)
				m_menuWindow->CloseWindow();
			if (m_helpWindow)
				m_helpWindow->CloseWindow();
		}

		xAux = (m_window->getSize().x - Constants::kFaceLenght) / 2;
		yAux = Constants::kBorderLenght + 2 * Constants::kTileLenght - Constants::kFaceLenght / 2;

		//tile pressing
		if (!m_gameField.GetIsFinished())
			if (!faceWasPressed &&
				mousePosition.x > Constants::kBorderLenght &&
				mousePosition.y > 2 * Constants::kBorderLenght + 3 * Constants::kTileLenght &&
				(uint16_t)mousePosition.x < m_window->getSize().x - Constants::kBorderLenght &&
				(uint16_t)mousePosition.y < m_window->getSize().y - Constants::kBorderLenght)
			{
				xMouse = (mousePosition.x - Constants::kBorderLenght) / Constants::kTileLenght;
				yMouse = (mousePosition.y - 2 * Constants::kBorderLenght - 3 * Constants::kTileLenght) / Constants::kTileLenght;
				tilePosition = { yMouse,xMouse };

				if (event.type == sf::Event::MouseButtonReleased)
				{
			        Logger::Log(std::cout, Logger::Level::INFO, "Tile released at:", "column", xMouse, "line", yMouse);
					faceState = Faces::HappyFace;
					if (event.key.code == sf::Mouse::Left)
					{
						if (firstClick == true)
						{
							start = std::chrono::high_resolution_clock::now();
							Logger::Log(std::cout, Logger::Level::INFO, "Timer stared...");
							m_gameField.SpawnBombs(tilePosition);
							m_gameField.FindAllNeighborBombs();
							firstClick = false;
						}
						if (m_gameField[tilePosition].GetState() == Tile::State::Hidden ||
							m_gameField[tilePosition].GetState() == Tile::State::Pressed ||
							m_gameField[tilePosition].GetState() == Tile::State::isHinted)
						{
							if (m_gameField[tilePosition].GetState() == Tile::State::Pressed)
								m_gameField[tilePosition].SetState(Tile::State::Hidden);
							if (m_gameField[tilePosition].GetState() == Tile::State::isHinted)
								m_gameField.SetHintNumber(m_gameField.GetHintNumber() - 1);

							m_gameField.RevealTiles(tilePosition);

							if (m_gameField.GetIsFinished() == true)
								faceState = Faces::DeadFace;
							else
							{
								m_gameField.VerifyGameWon();
								if (m_gameField.GetIsFinished() == true)
								{
									faceState = Faces::CoolFace;
									Logger::Log(std::cout, Logger::Level::INFO, "Game won in", time, "seconds...");
								}
							}
						}
						tileWasPressed = false;
					}
				}

				else if (event.type == sf::Event::MouseButtonPressed)
				{
					Logger::Log(std::cout, Logger::Level::INFO, "Tile pressed at:", "column", xMouse, "line", yMouse);
					if (event.key.code == sf::Mouse::Right)
					{


						if (m_gameField[tilePosition].GetState() == Tile::State::Hidden && !firstClick)
						{
							m_gameField[tilePosition].SetState(Tile::State::Flagged);
							m_gameField.SetNumberFlags(m_gameField.GetNumberFlags() + 1);
							Logger::Log(std::cout, Logger::Level::INFO, "Added a flag...");
						}
						else if (m_gameField[tilePosition].GetState() == Tile::State::Flagged)
						{
							m_gameField[tilePosition].SetState(Tile::State::Hidden);
							m_gameField.SetNumberFlags(m_gameField.GetNumberFlags() - 1);
							Logger::Log(std::cout, Logger::Level::INFO, "Removed a flag...");
						}
					}
					else if (event.key.code == sf::Mouse::Left)
					{
						if (m_gameField[tilePosition].GetState() == Tile::State::Hidden)
						{
							if (m_gameField[tilePosition].GetState() == Tile::State::isHinted)
								m_gameField.SetHintNumber(m_gameField.GetHintNumber() - 1);
							m_gameField[tilePosition].SetState(Tile::State::Pressed);
						}

						tileWasPressed = true;
						faceState = Faces::AmazedFace;

					}
				}
				if (tileWasPressed && previousTileIndex.first < m_gameField.GetHeight() &&
					previousTileIndex.second < m_gameField.GetWidth())
				{
					if (m_gameField[tilePosition].GetState() == Tile::State::Hidden)
						m_gameField[tilePosition].SetState(Tile::State::Pressed);
					if (m_gameField[previousTileIndex].GetState() == Tile::State::Pressed &&
						previousTileIndex != tilePosition)
						m_gameField[previousTileIndex].SetState(Tile::State::Hidden);
				}
				previousTileIndex = tilePosition;
			}
			else
			{
				if (tileWasPressed && m_gameField[previousTileIndex].GetState() == Tile::State::Pressed)
					m_gameField[previousTileIndex].SetState(Tile::State::Hidden);
				if (event.type == sf::Event::MouseButtonReleased)
				{
					tileWasPressed = false;
					faceState = Faces::HappyFace;
				}
			}

		//face pressing
		if (mousePosition.x > xAux && mousePosition.x < xAux + Constants::kFaceLenght &&
			mousePosition.y > yAux && mousePosition.y < yAux + Constants::kFaceLenght)
		{
			if (event.key.code == sf::Mouse::Left) {
				if (event.type == sf::Event::MouseButtonPressed)
				{
					Logger::Log(std::cout, Logger::Level::INFO, "Face pressed...");

					faceState = Faces::HappyPressedFace;
					faceWasPressed = true;
				}

				else if (event.type == sf::Event::MouseButtonReleased && faceWasPressed)
				{

					faceState = Faces::HappyFace;
					faceWasPressed = false;
					if (!firstClick)
					{
						m_gameField = MineField(m_gameField.GetWidth(), m_gameField.GetHeight(), m_gameField.GetNumberBombs());
						Logger::Log(std::cout, Logger::Level::INFO, "Started a new game...");
						firstClick = true;

					}
					else if (m_gameField.GetNumberFlags() != 0)
						m_gameField.RetrieveFlags();
					Logger::Log(std::cout, Logger::Level::INFO, "Face released...");
				}
			}
			if (faceWasPressed)
				faceState = Faces::HappyPressedFace;
		}
		else
		{
			if (faceWasPressed)
				faceState = Faces::HappyFace;
			if (event.type == sf::Event::MouseButtonReleased)
				faceWasPressed = false;
		}

		//menu button pressing
		if (m_menuButton.hasInBounds(mousePosition))
			if (event.type == sf::Event::MouseButtonPressed && !menuOpen)
			{
				Logger::Log(std::cout, Logger::Level::INFO, "Open menu window...");
				if (m_menuWindow == nullptr)
					m_menuWindow = std::make_shared<MenuWindow>();
				m_menuWindow->SetVisibile(true);
				if (m_menuWindow->Run(m_gameField))
				{
					firstClick = true;
					m_window = std::make_shared<sf::RenderWindow>(sf::VideoMode(m_gameField.GetWidth() * Constants::kTileLenght + 2 * Constants::kBorderLenght,
						m_gameField.GetHeight() * Constants::kTileLenght + 3 * Constants::kBorderLenght + 3 * Constants::kTileLenght),
						"Minesweeper", sf::Style::Close);
					faceState = Faces::HappyFace;
				}
				menuOpen = true;

				Logger::Log(std::cout, Logger::Level::INFO, "Closed menu window...");
			}

		//help button pressing
		if (m_helpButton.hasInBounds(mousePosition))
			if (event.type == sf::Event::MouseButtonPressed && !helpOpen)
			{
				Logger::Log(std::cout, Logger::Level::INFO, "Open help window...");				
					if (m_helpWindow == nullptr)
						m_helpWindow = std::make_shared<HelpWindow>();
					m_helpWindow->SetVisibile(true);
					m_helpWindow->Run();
				helpOpen = true;				
				Logger::Log(std::cout, Logger::Level::INFO, "Closed help window...");
				
			}

		//hint button pressing
		if (m_hintButton.hasInBounds(mousePosition))
			if (event.type == sf::Event::MouseButtonPressed && firstClick == false && !m_gameField.GetIsFinished())
				m_gameField.makeHint();

	}
	if (firstClick == true)
		time = 0;
	else
		if (m_gameField.GetIsFinished() == false)
		{
			auto end = std::chrono::high_resolution_clock::now();
			std::chrono::duration<float> duration = end - start;
			time = static_cast<uint16_t>(duration.count());
		}
}

void GameWindow::Run()
{
	while (m_window->isOpen())
	{
		static Faces faceState = Faces::HappyFace;
		static uint16_t time;

		ExecuteEvents(time, faceState);
		Draw(time, faceState);
	}
}

const uint16_t GameWindow::selectTile(const Tile& tile) const
{
	switch (tile.GetState())
	{
	case Tile::State::Hidden:
		return 9;
	case Tile::State::Flagged:
		return 10;
	case Tile::State::isHinted:
		return 14;
	case Tile::State::WrongFlagged:
		return 12;
	case Tile::State::Pressed:
		return 0;
	case Tile::State::Revealed:
		switch (tile.GetInformation())
		{
		case Tile::Information::RedBomb:
			return 11;
		case Tile::Information::Bomb:
			return 13;
		default:
			return static_cast<uint8_t>(tile.GetInformation());
		}
	default:
		return NULL;
	}
}

void GameWindow::drawTimer(const uint16_t timer, const std::function< void(uint16_t xPos, uint16_t yPos, uint8_t digitPos) >& drawDigit) const
{
	uint16_t valueY = Constants::kBorderLenght + 2 * Constants::kTileLenght - Constants::kDigitHeight / 2;
	uint16_t valueX = m_window->getSize().x - valueY - 3 * Constants::kDigitLenght + Constants::kTileLenght;

	drawDigit(valueX, valueY, timer / 100 % 10);
	drawDigit(valueX + Constants::kDigitLenght, valueY, timer / 10 % 10);
	drawDigit(valueX + 2 * Constants::kDigitLenght, valueY, timer % 10);
}

void GameWindow::drawFlagCounter(int16_t numberOfFlags, const std::function< void(uint16_t xPos,
	uint16_t yPos, uint8_t digitPos) >& drawDigit) const
{
	uint8_t value = Constants::kBorderLenght + 2 * Constants::kTileLenght - Constants::kDigitHeight / 2;
	if (numberOfFlags < 0)
	{
		drawDigit(value - Constants::kTileLenght, value, 10);
		numberOfFlags *= -1;
	}
	else
		drawDigit(value - Constants::kTileLenght, value, numberOfFlags / 100 % 10);

	drawDigit(value - Constants::kTileLenght + Constants::kDigitLenght, value, numberOfFlags / 10 % 10);
	drawDigit(value - Constants::kTileLenght + 2 * Constants::kDigitLenght, value, numberOfFlags % 10);
}
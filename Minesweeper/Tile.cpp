#include "Tile.h"

Tile::Tile()
	:Tile(Tile::State::Hidden, Tile::Information::Zero)
{
}

Tile::Tile(const State state, const Information information)
	: m_state{ state }, m_information{ information }
{
	static_assert(sizeof(*this) <= 4, "This class should be 1 byte in size.");
}

Tile::Tile(const Tile& tile)
{
	*this = tile;
}

Tile::Tile(Tile&& tile) noexcept
{
	*this = std::move(tile);
}

Tile& Tile::operator=(const Tile& tile)
{
	if (this != &tile)
	{
		m_state = tile.m_state;
		m_information = tile.m_information;
	}
	return *this;
}

Tile& Tile::operator=(Tile&& tile) noexcept
{
	if (this != &tile)
	{
		m_state = tile.m_state;
		m_information = tile.m_information;
		new(&tile) Tile();
	}
	return *this;
}

Tile::~Tile()
{
	m_state = Tile::State::Hidden;
	m_information = Tile::Information::Zero;
}

const Tile::State Tile::GetState()const
{
	return m_state;
}

void Tile::SetState(const State state)
{
	m_state = state;
}

const Tile::Information Tile::GetInformation() const
{
	return m_information;
}

void Tile::SetInformation(Information information)
{
	m_information = information;
}

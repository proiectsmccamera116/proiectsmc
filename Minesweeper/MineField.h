#pragma once

#include <vector>
#include <random>
#include <queue>
#include <utility>
#include <unordered_set>

#include "Tile.h"
#include "../Logging/Logging.h"

class MineField
{
public:
	using Position = std::pair<uint16_t, uint16_t>;
	static constexpr int16_t iIndex[] = { -1,-1,-1,0,0,1,1,1 };
	static constexpr int16_t jIndex[] = { -1,0,1,-1,1,-1,0,1 };

public:
	MineField();
	MineField(const uint16_t width,const uint16_t height,const uint16_t numberBombs);
	MineField(const MineField& minefield);
	MineField(MineField&& minefield) noexcept;
	MineField& operator=(const MineField& minefield);
	MineField& operator=(MineField&& minefield) noexcept;
	~MineField();

	void SpawnBombs(const Position& noBombTilePosition);
	void FindAllNeighborBombs();
	void RevealTiles(const Position& tile);
	void makeHint();
	void GameOver();
	void VerifyGameWon();
	void RetrieveFlags();

	const uint16_t GetNumberRevealed()const;
	void SetNumberRevealed(uint16_t numberRevealed);

	const int16_t GetNumberFlags()const;
	void SetNumberFlags(int16_t flags);

	void SetIsFinished(bool isFinished);
	const bool GetIsFinished()const;

	const uint16_t GetHintNumber()const;
	void SetHintNumber(uint16_t number);

	std::vector<Tile>& GetField();
	const uint16_t GetWidth()const;
	const uint16_t GetHeight()const;
	const int16_t GetNumberBombs()const;

	Tile& operator[](const Position& position);
	const Tile& operator[](const Position& position) const;
	
private:
	struct PositionHash
	{
		std::size_t operator() (const Position& position) const
		{
			return std::hash<uint16_t>()(position.first) ^ std::hash<uint16_t>()(position.second);
		}
	};
	using NeighboursSet = std::unordered_set<Position, PositionHash>;

private:
	void ChainRevealTiles(const uint16_t tile);
	const size_t Random() const;
	const void GetNeighboursOf(const Position& position, NeighboursSet& neighboursSet);

	std::vector <Tile> m_field;
	uint16_t m_width;
	uint16_t m_height;
	uint16_t m_numberBombs;
	uint16_t m_numberFlags;
	uint16_t m_numberRevealed;
	uint16_t m_hintNumber;
	bool m_isFinised;

	
};


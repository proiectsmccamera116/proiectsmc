#pragma once

#include "BasicWindow.h"
#include "Constants.h"
#include "Resource.h"

class HelpWindow : public BasicWindow
{
public:
	HelpWindow();
	HelpWindow(const HelpWindow& helpWindow);
	HelpWindow(HelpWindow&& helpWindow) noexcept;
	HelpWindow& operator=(const HelpWindow& helpWindow);
	HelpWindow& operator=(HelpWindow&& helpWindow) noexcept;
	~HelpWindow();

private:
	bool ExecuteEvents();
	void Draw();

	sf::View m_view;
};
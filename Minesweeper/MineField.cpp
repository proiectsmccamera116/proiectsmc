#include "MineField.h"

MineField::MineField()
	:MineField(0, 0, 0)
{
}

MineField::MineField(const uint16_t width,const uint16_t height,const uint16_t numberBombs)
	: m_height{ height }, m_width{ width }, m_numberBombs{ numberBombs },
	m_numberFlags{ 0 }, m_isFinised{ false }, m_numberRevealed{ 0 }, m_hintNumber{ 0 }
{
	m_field.resize((std::size_t)m_height* m_width);
	if (m_height && m_width && m_numberBombs)
		Logger::Log(std::cout, Logger::Level::INFO, "Created a game with", m_height, "height,", m_width, "width...");
}

MineField::MineField(const MineField& minefield)
{
	*this = minefield;
}

MineField::MineField(MineField&& minefield) noexcept
{
	*this = std::move(minefield);
}

MineField& MineField::operator=(const MineField& minefield)
{
	if (this != &minefield)
	{
		m_height = minefield.m_height;
		m_width = minefield.m_width;
		m_numberBombs = minefield.m_numberBombs;
		m_numberRevealed = minefield.m_numberRevealed;
		m_field = minefield.m_field;
		m_numberFlags = minefield.m_numberFlags;
		m_isFinised = minefield.m_isFinised;
		m_hintNumber = minefield.m_hintNumber;
	}
	return *this;
}

MineField& MineField::operator=(MineField&& minefield) noexcept
{
	if (this != &minefield)
	{
		m_height = minefield.m_height;
		m_width = minefield.m_width;
		m_field = minefield.m_field;
		m_numberBombs = minefield.m_numberBombs;
		m_numberFlags = minefield.m_numberFlags;
		m_numberRevealed = minefield.m_numberRevealed;
		m_isFinised = minefield.m_isFinised;
		m_hintNumber = minefield.m_hintNumber;
		new(&minefield) MineField(0, 0, 0);
	}
	return *this;
}

MineField::~MineField()
{
	m_height = 0;
	m_width = 0;
	m_numberBombs = 0;
	m_numberFlags = 0;
	m_numberRevealed = 0;
	m_hintNumber = 0;
	m_field.clear();
}

void MineField::SpawnBombs(const Position& noBombTilePosition)
{
	Logger::Log(std::cout, Logger::Level::INFO, "Spawned Bombs", m_numberBombs);

	static std::random_device random_device;
	static std::mt19937 random_engine(random_device());

	NeighboursSet neighboursSet;
	GetNeighboursOf(noBombTilePosition, neighboursSet);

	auto& [line, column] = noBombTilePosition;
	uint16_t noBombTileIndex = line * m_width + column;
	std::vector<uint16_t> vector;
	vector.reserve(m_field.size());
	size_t noBombSpace = m_field.size() - m_numberBombs;

	for (uint16_t indexField = 0; indexField < m_field.size(); ++indexField)
	{
		Position position{ indexField / m_width, indexField % m_width };
		if (indexField == noBombTileIndex)
			continue;

		if (noBombSpace > 1 && (neighboursSet.find(position)) != neighboursSet.end())
		{
			--noBombSpace;
			continue;
		}

		vector.push_back(indexField);
	}

	std::vector<uint16_t> indexes;
	indexes.reserve(m_numberBombs);
	std::sample(vector.begin(), vector.end(), std::back_inserter(indexes), m_numberBombs, random_engine);

	for (const auto& index : indexes)
	{
		m_field[index].SetInformation(Tile::Information::Bomb);
	}
}

void MineField::FindAllNeighborBombs()
{
	uint8_t aux;
	uint16_t neighborTile;
	uint16_t line;
	uint16_t column;
	for (uint16_t index1 = 0; index1 < m_field.size(); ++index1)
	{
		aux = 0;
		line = index1 / m_width;
		column = index1 % m_width;
		if (m_field[index1].GetInformation() == Tile::Information::Bomb)
			continue;
		for (uint16_t index2 = 0; index2 < sizeof(iIndex) / sizeof(iIndex[0]); ++index2)
		{
			if (line + iIndex[index2] < 0 || line + iIndex[index2] >= m_height ||
				column + jIndex[index2] < 0 || column + jIndex[index2] >= m_width)
				continue;

			neighborTile = (line + iIndex[index2]) * m_width + (column + jIndex[index2]);
			if (m_field[neighborTile].GetInformation() == Tile::Information::Bomb)
				++aux;
		}
		m_field[index1].SetInformation(static_cast<Tile::Information>(aux));

	}
	Logger::Log(std::cout, Logger::Level::INFO, "Found the neighbor bombs for every tile...");
}

void MineField::RevealTiles(const Position& tile)
{
	auto& [line, column] = tile;
	uint16_t indexFieldTile = line * m_width + column;

	if (m_field[indexFieldTile].GetInformation() == Tile::Information::Bomb)
	{
		Logger::Log(std::cout, Logger::Level::INFO, "Bomb pressed...");
		GameOver();
		m_field[indexFieldTile].SetInformation(Tile::Information::RedBomb);
		m_field[indexFieldTile].SetState(Tile::State::Revealed);
		m_isFinised = true;
		return;
	}
	if (m_field[indexFieldTile].GetInformation() != Tile::Information::Zero)
	{
		Logger::Log(std::cout, Logger::Level::INFO, "Revealed one tile...");
		++m_numberRevealed;
		m_field[indexFieldTile].SetState(Tile::State::Revealed);
		return;
	}
	ChainRevealTiles(indexFieldTile);
}

void MineField::makeHint()
{
	bool madeHint = false;
	size_t random = Random();
	while (!madeHint && (m_hintNumber < m_field.size() - m_numberRevealed - m_numberFlags ||
		m_numberBombs != m_numberFlags))
	{
		if (m_field[random].GetInformation() != Tile::Information::Bomb &&
			(m_field[random].GetState() == Tile::State::Hidden || m_field[random].GetState() == Tile::State::Flagged))
		{
			if (m_field[random].GetState() == Tile::State::Flagged)
				--m_numberFlags;
			m_field[random].SetState(Tile::State::isHinted);
			madeHint = true;
			Logger::Log(std::cout, Logger::Level::INFO, "Marked a hint...");
		}
		else
			if (m_field[random].GetInformation() == Tile::Information::Bomb &&
				m_field[random].GetState() == Tile::State::Hidden)
			{
				m_field[random].SetState(Tile::State::Flagged);
				madeHint = true;
				m_numberFlags++;
				m_numberRevealed--;
				Logger::Log(std::cout, Logger::Level::INFO, "Hinted a flag...");
			}
		random = Random();
		if (madeHint)
			m_hintNumber++;
	}
}

const uint16_t MineField::GetNumberRevealed()const
{
	return m_numberRevealed;
}

void MineField::SetNumberRevealed(uint16_t numberRevealed)
{
	m_numberRevealed = numberRevealed;
}

Tile& MineField::operator[](const Position& position)
{
	const auto& [line, column] = position;

	if (line >= m_height || column >= m_width)
	{
		Logger::Log(std::cout, Logger::Level::ERROR, "Position accesed out of bound...");
		throw "Board index out of bound.";
	}

	return m_field.at((std::size_t)line * m_width + column);
}

const Tile& MineField::operator[](const Position& position) const
{
	const auto& [line, column] = position;

	if (line >= m_height || column >= m_width)
	{
		Logger::Log(std::cout, Logger::Level::ERROR, "Position accesed out of bound...");
		throw "Board index out of bound.";
	}

	return m_field.at((std::size_t)line * m_width + column);
}

void MineField::ChainRevealTiles(const uint16_t indextile)
{
	uint16_t line;
	uint16_t column;
	uint16_t neighborTile;
	std::queue<uint16_t> searchQueue;
	searchQueue.push(indextile);
	++m_numberRevealed;
	while (!searchQueue.empty())
	{

		m_field[searchQueue.front()].SetState(Tile::State::Revealed);
		line = searchQueue.front() / m_width;
		column = searchQueue.front() % m_width;
		for (uint16_t index = 0; index < sizeof(iIndex) / sizeof(iIndex[0]); ++index)
		{
			if (line + iIndex[index] < 0 || line + iIndex[index] >= m_height ||
				column + jIndex[index] < 0 || column + jIndex[index] >= m_width)
				continue;

			neighborTile = (line + iIndex[index]) * m_width + (column + jIndex[index]);

			if (m_field[neighborTile].GetInformation() == Tile::Information::Zero &&
				(m_field[neighborTile].GetState() == Tile::State::Hidden ||
					m_field[neighborTile].GetState() == Tile::State::isHinted))
				searchQueue.push(neighborTile);

			if (m_field[neighborTile].GetState() == Tile::State::Hidden ||
				m_field[neighborTile].GetState() == Tile::State::isHinted)
			{
				if (m_field[neighborTile].GetState() == Tile::State::isHinted)
					--m_hintNumber;
				m_field[neighborTile].SetState(Tile::State::Revealed);
				++m_numberRevealed;
			}
		}
		searchQueue.pop();
	}
	Logger::Log(std::cout, Logger::Level::INFO, "Revealed multiple tiles...");
}

const int16_t MineField::GetNumberFlags()const
{
	return m_numberFlags;
}

const int16_t MineField::GetNumberBombs()const
{
	return m_numberBombs;
}

void MineField::SetNumberFlags(int16_t flags)
{
	m_numberFlags = flags;
}

const size_t MineField::Random() const
{
	static std::mt19937 random_engine(std::random_device{}());
	std::uniform_int_distribution<std::size_t> distribution(0, m_field.size() - 1);

	return distribution(random_engine);
}

const void MineField::GetNeighboursOf(const Position& position, NeighboursSet& neighboursSet)
{
	auto& [line, column] = position;
	Position neighbourPosition;

	for (uint16_t index = 0; index < sizeof(iIndex) / sizeof(iIndex[0]); ++index)
	{
		neighbourPosition = { line + iIndex[index], column + jIndex[index] };
		if (neighbourPosition.first > 0 || neighbourPosition.first < m_height ||
			neighbourPosition.second > 0 || neighbourPosition.second < m_width)
			neighboursSet.insert(neighbourPosition);
	}
}

void MineField::SetIsFinished(bool isFinished)
{
	m_isFinised = isFinished;
}

const bool MineField::GetIsFinished()const
{
	return m_isFinised;
}

 std::vector<Tile>& MineField::GetField()
{
	return m_field;
}

const uint16_t MineField::GetWidth()const
{
	return m_width;
}

const uint16_t MineField::GetHeight()const
{
	return m_height;
}

const uint16_t MineField::GetHintNumber()const
{
	return m_hintNumber;
}

void MineField::SetHintNumber(uint16_t number)
{
	m_hintNumber = number;
}

void MineField::GameOver()
{
	for (auto& tile : m_field)
		if (tile.GetInformation() != Tile::Information::Bomb)
		{
			if (tile.GetState() == Tile::State::Flagged)
				tile.SetState(Tile::State::WrongFlagged);
		}
		else
			if (tile.GetState() == Tile::State::Hidden)
				tile.SetState(Tile::State::Revealed);
	Logger::Log(std::cout, Logger::Level::INFO, "Game over...");
}

void MineField::VerifyGameWon()
{
	uint16_t countHiddenBombs = 0;

	for (const auto& tile : m_field)
		if (tile.GetState() == Tile::State::Hidden)
			++countHiddenBombs;

	if (m_numberBombs - countHiddenBombs - m_numberFlags == 0)
	{
		for (auto& tile : m_field)
			if (tile.GetState() == Tile::State::Hidden)
				tile.SetState(Tile::State::Flagged);
			else
				if (tile.GetState() == Tile::State::isHinted)
					tile.SetState(Tile::State::Revealed);

		m_isFinised = true;
		m_numberFlags = m_numberBombs;
	}
}

void MineField::RetrieveFlags()
{
	for (uint16_t index = 0; m_numberFlags != 0; ++index)
		if (m_field[index].GetState() == Tile::State::Flagged)
		{
			m_field[index].SetState(Tile::State::Hidden);
			--m_numberFlags;
		}
}

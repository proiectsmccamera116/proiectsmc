#pragma once

#include <cstdint>

class Constants
{
public:
	static constexpr uint8_t kTileLenght = 32;
	static constexpr uint8_t kBorderLenght = 20;
	static constexpr uint8_t kDigitLenght = 26;
	static constexpr uint8_t kDigitHeight = 46;
	static constexpr uint8_t kFaceLenght = 52;
	static constexpr uint8_t rulesImageOffSet = 26;

	static constexpr uint8_t kBeginnerHeight = 9;
	static constexpr uint8_t kBeginnerWidth = 9;
	static constexpr uint8_t kBeginnerMines = 10;

	static constexpr uint8_t kIntermediateHeight = 16;
	static constexpr uint8_t kIntermediateWidth = 16;
	static constexpr uint8_t kIntermediateMines = 40;

	static constexpr uint8_t kExpertHeight = 16;
	static constexpr uint8_t kExpertWidth = 30;
	static constexpr uint8_t kExpertMines = 99;

	static constexpr uint16_t kHelpWindowX = 623;
	static constexpr uint16_t kHelpWindowY = 1126;
	static constexpr uint16_t kHelpWindowViewSize = 400;
	static constexpr uint16_t kMaxFullView=(kHelpWindowY-kHelpWindowViewSize)-(kHelpWindowY%100);
	static constexpr uint16_t scrollSize = 100;
};
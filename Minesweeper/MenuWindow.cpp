#include "MenuWindow.h"
#include <iostream>


MenuWindow::MenuWindow()
	:BasicWindow(sf::VideoMode(450, 300), "Menu", sf::Style::Close),
	m_difficulty{ Difficulty::None }, m_customDimensions{ CustomDimensions::None }
{
	m_beginnerText.setPosition({ 50,50 });
	m_intermediateText.setPosition({ 50,100 });
	m_expertText.setPosition({ 50,150 });
	m_customText.setPosition({ 50,200 });
	m_dimensionsText.setPosition({ 200, 10 });

	m_beginnerText.setFillColor(sf::Color::Black);
	m_intermediateText.setFillColor(sf::Color::Black);
	m_expertText.setFillColor(sf::Color::Black);
	m_customText.setFillColor(sf::Color::Black);
	m_dimensionsText.setFillColor(sf::Color::Black);

	m_heightCustom.setFillColor(sf::Color::White);
	m_widthCustom.setFillColor(sf::Color::White);
	m_minesCustom.setFillColor(sf::Color::White);
}

MenuWindow::MenuWindow(const MenuWindow& menuWindow)
{
	*this = menuWindow;
}

MenuWindow::MenuWindow(MenuWindow&& menuWindow) noexcept
{
	*this = std::move(menuWindow);
}

MenuWindow& MenuWindow::operator=(const MenuWindow& menuWindow)
{
	if (this != &menuWindow)
		m_window = menuWindow.m_window;
	return *this;
}

MenuWindow& MenuWindow::operator=(MenuWindow&& menuWindow) noexcept
{
	if (this != &menuWindow)
	{
		m_window = menuWindow.m_window;
		new(&menuWindow) MenuWindow();
	}
	return *this;
}

MenuWindow::~MenuWindow()
{
}

const sf::Text MenuWindow::GetBeginnerText()const
{
	return m_beginnerText;
}

const sf::Text MenuWindow::GetIntermediateText()const
{
	return m_intermediateText;
}

const sf::Text MenuWindow::GetExpertText()const
{
	return m_expertText;
}

const sf::Text MenuWindow::GetCustomText()const
{
	return m_customText;
}

const sf::Text MenuWindow::GetDimensionsText()const
{
	return m_dimensionsText;
}

const Button MenuWindow::GetHeightCustom()const
{
	return m_heightCustom;
}

const Button MenuWindow::GetWidthCustom()const
{
	return m_widthCustom;
}

const Button MenuWindow::GetMinesCustom()const
{
	return m_minesCustom;
}

void MenuWindow::Decolor(const Difficulty difficulty)
{
	switch (difficulty)
	{
	case Difficulty::Beginner:
		m_beginnerButton.setFillColor(sf::Color::Transparent);
		break;
	case Difficulty::Intermediate:
		m_intermediateButton.setFillColor(sf::Color::Transparent);
		break;
	case Difficulty::Expert:
		m_expertButton.setFillColor(sf::Color::Transparent);
		break;
	case Difficulty::Custom:
		m_customButton.setFillColor(sf::Color::Transparent);
		break;
	default:
		break;
	}
}

void MenuWindow::DecolorCustomDimensions()
{
	m_heightCustom.setFillColor(sf::Color::White);
	m_widthCustom.setFillColor(sf::Color::White);
	m_minesCustom.setFillColor(sf::Color::White);
}

bool MenuWindow::ExecuteEvents(MineField& mineField, bool& changedMinefield)
{
	sf::Event event;
	sf::Vector2i mousePosition = sf::Mouse::getPosition(*m_window);

	while (m_window->pollEvent(event))
	{
		if (event.type == sf::Event::Closed)
		{
			m_window->setVisible(false);
			return false;
		}

		//select dificulty
		if (event.type == sf::Event::MouseButtonPressed)
		{
			if (m_beginnerButton.hasInBounds(mousePosition))
			{
				Decolor(m_difficulty);
				if (m_difficulty != Difficulty::Beginner)
				{
					m_difficulty = Difficulty::Beginner;
					m_beginnerButton.setFillColor(sf::Color::Red);
					Logger::Log(std::cout, Logger::Level::INFO, "Beginner is selected...");
				}
				else
				{
					m_difficulty = Difficulty::None;
					Logger::Log(std::cout, Logger::Level::INFO, "No difficulty selected...");
				}
			}
			else if (m_intermediateButton.hasInBounds(mousePosition))
			{
				Decolor(m_difficulty);
				if (m_difficulty != Difficulty::Intermediate)
				{
					m_difficulty = Difficulty::Intermediate;
					m_intermediateButton.setFillColor(sf::Color::Red);
					Logger::Log(std::cout, Logger::Level::INFO, "Intermediate is selected...");
				}
				else
				{
					m_difficulty = Difficulty::None;
					Logger::Log(std::cout, Logger::Level::INFO, "No difficulty selected...");
				}
			}
			else if (m_expertButton.hasInBounds(mousePosition))
			{
				Decolor(m_difficulty);
				if (m_difficulty != Difficulty::Expert)
				{
					m_difficulty = Difficulty::Expert;
					m_expertButton.setFillColor(sf::Color::Red); 
					Logger::Log(std::cout, Logger::Level::INFO, "Expert is selected...");
				}
				else
				{
					m_difficulty = Difficulty::None;
					Logger::Log(std::cout, Logger::Level::INFO, "No difficulty selected...");
				}
			}
			else if (m_customButton.hasInBounds(mousePosition))
			{
				Decolor(m_difficulty);
				if (m_difficulty != Difficulty::Custom)
				{
					m_difficulty = Difficulty::Custom;
					m_customButton.setFillColor(sf::Color::Red);
					Logger::Log(std::cout, Logger::Level::INFO, "Custom is selected...");
				}
				else
				{
					m_difficulty = Difficulty::None;
					Logger::Log(std::cout, Logger::Level::INFO, "No difficulty selected...");
				}
			}

			if (m_heightCustom.hasInBounds(mousePosition))
			{
				m_customDimensions = CustomDimensions::Height;
				DecolorCustomDimensions();
				m_heightCustom.setFillColor(sf::Color{255,255,150});

			}
			else if (m_widthCustom.hasInBounds(mousePosition))
			{
				m_customDimensions = CustomDimensions::Width;
				DecolorCustomDimensions();
				m_widthCustom.setFillColor(sf::Color{ 255,255,150});
			}
			else if (m_minesCustom.hasInBounds(mousePosition))
			{
				m_customDimensions = CustomDimensions::Mines;
				DecolorCustomDimensions();
				m_minesCustom.setFillColor(sf::Color{ 255,255,150});
			}
			else
			{
				m_customDimensions = CustomDimensions::None;
				DecolorCustomDimensions();
			}
		}
		if (event.type == sf::Event::MouseButtonReleased)
			if (m_newGame.hasInBounds(mousePosition))
			{
				switch (m_difficulty)
				{
				case Difficulty::Beginner:
					mineField = MineField{ Constants::kBeginnerWidth,Constants::kBeginnerHeight,Constants::kBeginnerMines };
					m_window->setVisible(false);
					changedMinefield = true;
					return  false;

				case Difficulty::Intermediate:
					mineField = MineField{ Constants::kIntermediateWidth,Constants::kIntermediateHeight,Constants::kIntermediateMines };
					m_window->setVisible(false);
					changedMinefield = true;
					return  false;

				case Difficulty::Expert:
					mineField = MineField{ Constants::kExpertWidth,Constants::kExpertHeight,Constants::kExpertMines };
					m_window->setVisible(false);
					changedMinefield = true;
					return  false;

				case Difficulty::Custom:
					if (m_widthCustom.getString().empty() || m_heightCustom.getString().empty() || m_minesCustom.getString().empty())
						return true;
					mineField = MineField{ (uint16_t)std::stoi(m_widthCustom.getString()),(uint16_t)std::stoi(m_heightCustom.getString()),(uint16_t)std::stoi(m_minesCustom.getString()) };
					m_window->setVisible(false);
					changedMinefield = true;
					return  false;

				default:
					return  true;
				}
			}
		if (event.type == sf::Event::TextEntered && (event.text.unicode > 47 &&
			event.text.unicode < 58 || event.text.unicode == '\b'))
		{
			static std::string heightInput;
			static std::string widthInput;
			static std::string minesInput;
			if (m_customDimensions == CustomDimensions::Height)
			{
				if (event.text.unicode == '\b')
				{
					if (heightInput.size())
						heightInput.erase(heightInput.size() - 1);
				}
				else
					heightInput += event.text.unicode;

				if (heightInput.size() && std::stoi(heightInput) > 24)
					heightInput = "24";
				else if (heightInput == "0")
					heightInput = "1";
				m_heightCustom.setString(heightInput);
			}
			if (m_customDimensions == CustomDimensions::Width)
			{
				if (event.text.unicode == '\b')
				{
					if (widthInput.size())
						widthInput.erase(widthInput.size() - 1);
				}
				else
					widthInput += event.text.unicode;

				if (widthInput.size() && std::stoi(widthInput) > 52)
					widthInput = "52";
				else if (widthInput == "0")
					widthInput = "1";
				m_widthCustom.setString(widthInput);
			}
			if (m_customDimensions == CustomDimensions::Mines && heightInput.size() && widthInput.size())
			{
				if (std::stoi(widthInput) < 8)
				{
					widthInput = "8";
					m_widthCustom.setString(widthInput);
				}
				if (event.text.unicode == '\b')
				{
					if (minesInput.size())
						minesInput.erase(minesInput.size() - 1);
				}
				else
					minesInput += event.text.unicode;

				if (minesInput.size() && std::stoi(heightInput) * std::stoi(widthInput) - 1 < std::stoi(minesInput))
					minesInput = std::to_string(std::stoi(heightInput) * std::stoi(widthInput) - 1);
				else if (minesInput == "0")
					minesInput = "1";

				m_minesCustom.setString(minesInput);
			}
		}
	}
	return true;

}

void MenuWindow::Draw()
{
	m_window->draw(m_beginnerButton);
	m_window->draw(m_intermediateButton);
	m_window->draw(m_expertButton);
	m_window->draw(m_customButton);
	m_window->draw(m_beginnerText);
	m_window->draw(m_intermediateText);
	m_window->draw(m_expertText);
	m_window->draw(m_customText);
	m_window->draw(m_dimensionsText);
	m_window->draw(m_heightCustom);
	m_window->draw(m_widthCustom);
	m_window->draw(m_minesCustom);
	m_window->draw(m_newGame);

	m_window->display();
	m_window->clear(sf::Color{ 192,192,192 });
}

bool MenuWindow::Run(MineField& mineField)
{
	bool active = true;
	bool changedMinefield = false;
	while (m_window->isOpen() && active)
	{
		active = ExecuteEvents(mineField, changedMinefield);
		Draw();
	}
	return changedMinefield;
}

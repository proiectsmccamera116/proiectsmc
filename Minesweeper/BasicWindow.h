#pragma once

#include <SFML/Graphics.hpp>
#include <memory>

class BasicWindow
{
public:
	BasicWindow();
	BasicWindow(const sf::VideoMode mode, const sf::String& title,const sf::Uint32 style = sf::Style::Default,
		const sf::ContextSettings& settings = sf::ContextSettings());
	BasicWindow(const BasicWindow& basicWindow);
	BasicWindow(BasicWindow&& basicWindow) noexcept;
	BasicWindow& operator=(const BasicWindow& basicWindow);
	BasicWindow& operator=(BasicWindow&& basicWindow) noexcept;
	~BasicWindow();

	void SetVisibile(const bool visible);
	virtual void Run();
	void CloseWindow();

protected:
	virtual bool ExecuteEvents();
	virtual void Draw();
	std::shared_ptr<sf::RenderWindow> m_window;
};


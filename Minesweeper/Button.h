#pragma once

#include <SFML/Graphics.hpp>
#include <string>

#include "Resource.h"

class Button :public sf::Drawable
{
public:
	Button();
	Button(const std::string& text, const sf::Vector2f& position, const sf::Vector2f& dimensions = { 0,0 });
	Button(const Button& button);
	Button(Button&& button) noexcept;
	Button& operator=(const Button& button);
	Button& operator=(Button&& button) noexcept;
	~Button();

	void draw(sf::RenderTarget& target, sf::RenderStates states) const;

	void setString(const std::string& text);
	void setFillColor(const sf::Color& color);
	void setTextColor(const sf::Color& color);
	void setPosition(const sf::Vector2f& position);
	void setCharacterSize(const uint16_t size);

	const uint16_t getCharacterSize() const;
	const std::string getString() const;
	const sf::Font getFont() const;
	const sf::Color getFillColor() const;
	const sf::Color getTextColor() const;
	const sf::Vector2f getPositon() const;
	const sf::Vector2f getSize() const;

	void ResizeToText();
	bool hasInBounds(const sf::Vector2i& Position) const;

private:
	sf::Text m_text;
	sf::RectangleShape m_textBorder;
};


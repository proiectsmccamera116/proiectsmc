#pragma once

#include <SFML/Graphics.hpp>
#include <functional>
#include <chrono>
#include "HelpWindow.h"
#include "MineField.h"
#include "MenuWindow.h"
#include "Constants.h"
#include "Button.h"
#include "Resource.h"
#include "../Logging/Logging.h"

class GameWindow : public BasicWindow
{
public:
	enum class Faces :uint8_t
	{
		HappyFace,
		HappyPressedFace,
		AmazedFace,
		DeadFace,
		CoolFace
	};

	GameWindow();
	GameWindow(MineField game);
	GameWindow(const GameWindow& other);
	GameWindow(GameWindow&& other) noexcept;
	GameWindow& operator=(const GameWindow& other);
	GameWindow& operator=(GameWindow&& other) noexcept;
	~GameWindow();

	void Run();

private:
	MineField m_gameField;

	Button m_menuButton;
	Button m_helpButton;
	Button m_hintButton;

	std::shared_ptr<MenuWindow> m_menuWindow;
	std::shared_ptr<HelpWindow> m_helpWindow;

	void Draw(uint16_t& time, Faces& faceState);
	void ExecuteEvents(uint16_t& time, Faces& faceState);

	const uint16_t selectTile(const Tile& tile) const;
	void drawTimer(const uint16_t, const std::function< void(uint16_t, uint16_t, uint8_t) >&) const;
	void drawFlagCounter(int16_t, const std::function< void(uint16_t, uint16_t, uint8_t) >&) const;
	
	void Load() const;
};


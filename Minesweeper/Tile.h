#pragma once

#include <cstdint>
#include <iostream>

class Tile
{
public:

	enum class State :uint8_t
	{
		Hidden,
		Flagged,
		Revealed,
		WrongFlagged,
		Pressed,
		isHinted
	};

	enum class Information :uint8_t
	{
		Zero,
		One,
		Two,
		Three,
		Four,
		Five,
		Six,
		Seven,
		Eight,
		Bomb,
		RedBomb
	};

public:
	Tile();
	Tile(const State state, const Information information);
	Tile(const Tile& tile);
	Tile(Tile&& tile) noexcept;
	~Tile();

	Tile& operator=(const Tile& tile);
	Tile& operator=(Tile&& tile) noexcept;

	const State GetState()const;
	void SetState(const State state);
	const Information GetInformation()const;
	void SetInformation(const Information information);

private:
	State m_state : 3;
	Information m_information : 4;
};


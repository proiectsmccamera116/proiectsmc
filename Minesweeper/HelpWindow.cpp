#include "HelpWindow.h"

HelpWindow::HelpWindow() :BasicWindow(sf::VideoMode(Constants::kHelpWindowX, Constants::kHelpWindowViewSize), "Help", sf::Style::Close)
{
	m_view = sf::View(sf::FloatRect(0, 0, Constants::kHelpWindowX, Constants::kHelpWindowViewSize));
}

HelpWindow::HelpWindow(const HelpWindow& helpWindow)
{
	*this = helpWindow;
}

HelpWindow::HelpWindow(HelpWindow&& helpWindow) noexcept
{
	*this = std::move(helpWindow);
}

HelpWindow& HelpWindow::operator=(const HelpWindow& helpWindow)
{
	if (this != &helpWindow)
	{
		m_window = helpWindow.m_window;
	}
	return *this;
}

HelpWindow& HelpWindow::operator=(HelpWindow&& helpWindow) noexcept
{
	if (this != &helpWindow)
	{
		m_window = helpWindow.m_window;
		new(&helpWindow) HelpWindow();
	}
	return *this;
}

HelpWindow::~HelpWindow()
{
}

bool HelpWindow::ExecuteEvents()
{
	sf::Event event;
	m_window->setView(m_view);
	static float yCoord = 0;
	while (m_window->pollEvent(event))
	{
		if (event.type == sf::Event::MouseWheelMoved)
		{
			//first in range
			if (yCoord + event.mouseWheel.delta * -Constants::scrollSize <= Constants::kMaxFullView &&
				yCoord + event.mouseWheel.delta * -Constants::scrollSize >= 0)
			{
				if (yCoord + event.mouseWheel.delta * -Constants::scrollSize == Constants::kMaxFullView)
				{
					m_view.move(0, (float)(event.mouseWheel.delta * -Constants::scrollSize + Constants::rulesImageOffSet));
					yCoord += event.mouseWheel.delta * -Constants::scrollSize + Constants::rulesImageOffSet;
				}
				else
				{
					m_view.move(0, (float)event.mouseWheel.delta * -Constants::scrollSize);
					yCoord += event.mouseWheel.delta * -Constants::scrollSize;
				}
			}

			//then a bit off range but not too much
			else
			{
				if (yCoord == Constants::rulesImageOffSet)
				{
					m_view.move(0, (float)-Constants::rulesImageOffSet);
					yCoord = 0;
				}
				else if (yCoord >= Constants::kMaxFullView - Constants::scrollSize && yCoord <= Constants::kMaxFullView)
				{
					m_view.move(0, Constants::kMaxFullView - yCoord + Constants::rulesImageOffSet);
					yCoord = Constants::kMaxFullView + Constants::rulesImageOffSet;
				}
				else if (yCoord <= Constants::scrollSize && yCoord >= 0)
				{
					m_view.move(0, -yCoord);
					yCoord = 0;
				}
			}
		}

		if (event.type == sf::Event::Closed)
		{
			m_window->setVisible(false);
			return false;
		}
	}
	return true;
}

void HelpWindow::Draw()
{
	static sf::Sprite rules(Resource::rulesTexture);
	m_window->draw(rules);
	m_window->display();
}
#include "BasicWindow.h"

BasicWindow::BasicWindow()
	:BasicWindow(sf::VideoMode(0,0), "BasicWindow", sf::Style::Close)
{	
}

BasicWindow::BasicWindow(const sf::VideoMode mode, const sf::String& title,const sf::Uint32 style,
	const sf::ContextSettings& settings)
{
	m_window = std::make_shared<sf::RenderWindow>();
	m_window->create(mode, title, style, settings);
	m_window->setFramerateLimit(30);
	m_window->setVisible(false);
}

BasicWindow::BasicWindow(const BasicWindow& basicWindow)
{
	*this = basicWindow;
}

BasicWindow::BasicWindow(BasicWindow&& basicWindow) noexcept
{
	*this = std::move(basicWindow);
}

BasicWindow& BasicWindow::operator=(const BasicWindow& basicWindow)
{
	if (this != &basicWindow)
		m_window = basicWindow.m_window;
	return *this;
}

BasicWindow& BasicWindow::operator=(BasicWindow&& basicWindow) noexcept
{
	if (this != &basicWindow)
	{
		m_window = basicWindow.m_window;
		new(&basicWindow) BasicWindow();
	}
	return *this;
}

BasicWindow::~BasicWindow()
{
}

void BasicWindow::SetVisibile(const bool visible)
{
	m_window->setVisible(visible);
}

void BasicWindow::Run()
{
	bool active = true;
	while (m_window->isOpen() && active)
	{
		active = ExecuteEvents();
		Draw();
	}
}

bool BasicWindow::ExecuteEvents()
{
	sf::Event event;
	while (m_window->pollEvent(event))
	{
		if (event.type == sf::Event::Closed)
		{
			m_window->close();
			return false;
		}
	}
	return true;
}

void BasicWindow::Draw()
{
}

void BasicWindow::CloseWindow()
{
	m_window->close();
}


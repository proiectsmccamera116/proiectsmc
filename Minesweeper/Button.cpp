#include "Button.h"

Button::Button()
	:Button("", { 0,0 })
{
}

Button::Button(const std::string& text, const sf::Vector2f& position, const sf::Vector2f& dimensions)
{
	m_text.setString(text);
	m_text.setCharacterSize(20);
	m_text.setFont(Resource::arial);
	m_text.setFillColor(sf::Color::Black);
	m_text.setPosition(position);

	if (dimensions.x == 0 && dimensions.y == 0)
		m_textBorder.setSize(sf::Vector2f(m_text.findCharacterPos(text.size()).x - 2 - position.x, 26.0f));
	else
		m_textBorder.setSize(dimensions);
	m_textBorder.setFillColor(sf::Color::Transparent);
	m_textBorder.setOutlineColor(sf::Color::Black);
	m_textBorder.setOutlineThickness(2);
	m_textBorder.setPosition(position);
}

Button::Button(const Button& button)
{
	*this = button;
}

Button::Button(Button&& button) noexcept
{
	*this = std::move(button);
}

Button::~Button()
{
}

Button& Button::operator=(const Button& button)
{
	if (this != &button)
	{
		m_text = button.m_text;
		m_textBorder = button.m_textBorder;
	}
	return *this;
}

Button& Button::operator=(Button&& button) noexcept
{
	if (this != &button)
	{
		m_text = button.m_text;
		m_textBorder = button.m_textBorder;
		new(&button) Button("", { 0,0 });
	}
	return *this;
}

void Button::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	target.draw(m_textBorder);
	target.draw(m_text);
}

void Button::setString(const std::string& text)
{
	m_text.setString(text);
}

void Button::setFillColor(const sf::Color& color)
{
	m_textBorder.setFillColor(color);
}

void Button::setTextColor(const sf::Color& color)
{
	m_text.setFillColor(color);
}

void Button::setPosition(const sf::Vector2f& position)
{
	m_text.setPosition(position);
	m_textBorder.setPosition(position);
}

void Button::setCharacterSize(uint16_t size)
{
	m_text.setCharacterSize(size);
}

const std::string Button::getString() const
{
	return m_text.getString();
}

const sf::Color Button::getFillColor() const
{
	return m_textBorder.getFillColor();
}

const sf::Color Button::getTextColor() const
{
	return m_text.getFillColor();
}

const sf::Vector2f Button::getPositon() const
{
	return m_text.getPosition();
}

const uint16_t Button::getCharacterSize() const
{
	return m_text.getCharacterSize();
}

const sf::Vector2f Button::getSize() const
{
	return m_textBorder.getSize();
}

const sf::Font Button::getFont() const
{
	return Resource::arial;
}

void Button::ResizeToText()
{
	m_textBorder.setSize(sf::Vector2f(m_text.findCharacterPos(m_text.getString().getSize()).x - 2 - m_textBorder.getPosition().x, 26.0f));
}

bool Button::hasInBounds(const sf::Vector2i& position) const
{
	return  position.x > m_textBorder.getPosition().x && position.y > m_textBorder.getPosition().y &&
		position.x < m_textBorder.getPosition().x + m_textBorder.getSize().x &&
		position.y < m_textBorder.getPosition().y + m_textBorder.getSize().y;
}

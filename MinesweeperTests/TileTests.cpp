#include "pch.h"
#include "CppUnitTest.h"

#include "../Minesweeper/Tile.h"
#include "../Minesweeper/Tile.cpp"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace MinesweeperTests
{
	TEST_CLASS(TileTests)
	{
	public:
		
		TEST_METHOD(Constructor)
		{
			Tile tile(Tile::State::Hidden, Tile::Information::Bomb);
			Assert::IsTrue(Tile::State::Hidden == tile.GetState());
			Assert::IsTrue(Tile::Information::Bomb == tile.GetInformation());
		}
	};
}

#include "pch.h"
#include "CppUnitTest.h"

#include "../Minesweeper/Minefield.h"
#include "../Minesweeper/MineField.cpp"
#include "../Logging/Logging.h"
#include "../Logging/Logging.cpp"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace MinesweeperTests
{
	TEST_CLASS(MineFieldTests)
	{
	public:

		TEST_METHOD(Constructor)
		{
			MineField mineField(8, 10, 20);
			Assert::IsTrue(mineField.GetWidth() == 8);
			Assert::IsTrue(mineField.GetHeight() == 10);
			Assert::IsTrue(mineField.GetNumberBombs() == 20);
			Assert::IsTrue(mineField.GetNumberFlags() == 0);
			Assert::IsTrue(mineField.GetIsFinished() == false);
			Assert::IsTrue(mineField.GetNumberRevealed() == 0);
			Assert::IsTrue(mineField.GetHintNumber() == 0);
		}
		TEST_METHOD(CorrectSpawnningOfBombs)
		{
			MineField mineField(8, 10, 15);
			mineField.SpawnBombs({ 2,4 });
			Assert::IsTrue(mineField[{2,4}].GetInformation() != Tile::Information::Bomb);
			uint16_t bombCounter = 0;
			for (auto& tile : mineField.GetField())
				if (tile.GetInformation() == Tile::Information::Bomb)
					++bombCounter;
			Assert::IsTrue(bombCounter == 15);
		}
		TEST_METHOD(NumberOfNeighbours)
		{
			MineField mineField(8, 10, 15);
			mineField.SpawnBombs({ 2,4 });
			mineField.FindAllNeighborBombs();

			uint8_t aux;
			uint16_t neighborTile;
			for (uint16_t index1 = 0; index1 < mineField.GetField().size(); ++index1)
			{
				aux = 0;
				uint16_t line = index1 / mineField.GetWidth();
				uint16_t column = index1 % mineField.GetWidth();
				if (mineField.GetField()[index1].GetInformation() == Tile::Information::Bomb)
					continue;
				for (uint16_t index2 = 0; index2 < sizeof(mineField.iIndex); ++index2)
				{
					if (line + mineField.iIndex[index2] < 0 || line + mineField.iIndex[index2] >= mineField.GetHeight() ||
						column + mineField.jIndex[index2] < 0 || column + mineField.jIndex[index2] >= mineField.GetWidth())
						continue;

					neighborTile = (line + mineField.iIndex[index2]) * mineField.GetWidth() + (column + mineField.jIndex[index2]);
					if (mineField.GetField()[neighborTile].GetInformation() == Tile::Information::Bomb)
						++aux;
				}

				Assert::IsTrue(static_cast<Tile::Information>(aux) == mineField.GetField()[index1].GetInformation());
			}
		}
		TEST_METHOD(GameOver)
		{
			MineField mineField(8, 10, 15);
			mineField.SpawnBombs({ 2,4 });
			mineField.FindAllNeighborBombs();
			mineField.GameOver();
			for (auto& tile : mineField.GetField())
				if (tile.GetInformation() == Tile::Information::Bomb)
					Assert::IsTrue(tile.GetState() == Tile::State::Revealed);

		}
		TEST_METHOD(GameWon)
		{
			MineField mineField(8, 10, 15);
			mineField.SpawnBombs({ 2,4 });
			mineField.FindAllNeighborBombs();
			for (auto& tile : mineField.GetField())
				if (tile.GetInformation() != Tile::Information::Bomb)
					tile.SetState(Tile::State::Revealed);
			mineField.VerifyGameWon();
			for (auto& tile : mineField.GetField())
				if (tile.GetInformation() == Tile::Information::Bomb)
					Assert::IsTrue(tile.GetState() == Tile::State::Flagged);
		}
		TEST_METHOD(GetAtOneOneConst)
		{
			const MineField minefield{ 8,8,10 };
			MineField::Position position{ 1, 1 };
			MineField& secondMinefield = const_cast<MineField&>(minefield);
			Assert::IsTrue(secondMinefield[position].GetState() == Tile::State::Hidden);
		}
		TEST_METHOD(GetAtMinusOneOne)
		{
			MineField mineField{ 8,8,10 };
			MineField::Position position{ -1, 1 };

			Assert::ExpectException<const char*>([&]() {
				mineField[position];
				});
		}
		TEST_METHOD(GetAtMinusOneOneConst)
		{
			const MineField mineField{ 8,8,10 };
			MineField::Position position{ -1, 1 };

			Assert::ExpectException < const char* > ([&]() {
				mineField[position];
				});
		}
		TEST_METHOD(GetAtOneMinusOneConst)
		{
			const MineField mineField{ 8,8,10 };
			MineField::Position position{ 1, -1 };

			Assert::ExpectException<const char*>([&]() {
				mineField[position];
				});
		}
		TEST_METHOD(GetAtOneMinusOne)
		{
			MineField mineField{ 8,8,10 };
			MineField::Position position{ 1, -1 };

			Assert::ExpectException<const char*>([&]() {
				mineField[position];
				});
		}

		TEST_METHOD(InitialBoardTilesInformation)
		{
			MineField mineField{8,8,10};
			for (auto& tile : mineField.GetField())
				Assert::IsTrue(tile.GetInformation()== Tile::Information::Zero);

		}

		TEST_METHOD(AllTilesBombsMinusOne)
		{
			MineField mineField{ 8,4,31 };
			MineField::Position userInput = {2,3};
			mineField.SpawnBombs(userInput);
			mineField.FindAllNeighborBombs();
			mineField.RevealTiles(userInput);
			mineField.VerifyGameWon();
			for (auto& tile : mineField.GetField())
				if (tile.GetInformation() == Tile::Information::Bomb)
					Assert::IsTrue(tile.GetState() == Tile::State::Flagged);


		}

		TEST_METHOD(WinOnUpperLeftCornerFourTilesFree)
		{
			MineField mineField{ 8,4,28 };
			MineField::Position userInput = { 0,0 };
			mineField.SpawnBombs(userInput);
			mineField.FindAllNeighborBombs();
			mineField.RevealTiles(userInput);
			mineField.VerifyGameWon();
			for (auto& tile : mineField.GetField())
				if (tile.GetInformation() == Tile::Information::Bomb)
					Assert::IsTrue(tile.GetState() == Tile::State::Flagged);
		}
		TEST_METHOD(WinOnUpperRightCornerFourTilesFree)
		{
			MineField mineField{ 8,4,28 };
			MineField::Position userInput = { 0,7 };
			mineField.SpawnBombs(userInput);
			mineField.FindAllNeighborBombs();
			mineField.RevealTiles(userInput);
			mineField.VerifyGameWon();
			for (auto& tile : mineField.GetField())
				if (tile.GetInformation() == Tile::Information::Bomb)
					Assert::IsTrue(tile.GetState() == Tile::State::Flagged);
		}
		TEST_METHOD(WinOnLowerLeftCornerFourTilesFree)
		{
			MineField mineField{ 8,4,28 };
			MineField::Position userInput = { 3,0 };
			mineField.SpawnBombs(userInput);
			mineField.FindAllNeighborBombs();
			mineField.RevealTiles(userInput);
			mineField.VerifyGameWon();
			for (auto& tile : mineField.GetField())
				if (tile.GetInformation() == Tile::Information::Bomb)
					Assert::IsTrue(tile.GetState() == Tile::State::Flagged);
		}
		TEST_METHOD(WinOnLowerRightCornerFourTilesFree)
		{
			MineField mineField{ 8,4,28 };
			MineField::Position userInput = { 3,7 };
			mineField.SpawnBombs(userInput);
			mineField.FindAllNeighborBombs();
			mineField.RevealTiles(userInput);
			mineField.VerifyGameWon();
			for (auto& tile : mineField.GetField())
				if (tile.GetInformation() == Tile::Information::Bomb)
					Assert::IsTrue(tile.GetState() == Tile::State::Flagged);
		}
		TEST_METHOD(WinOnMiddleSectionNineTilesFree)
		{
			MineField mineField{ 8,4,23 };
			MineField::Position userInput = { 2,4 };
			mineField.SpawnBombs(userInput);
			mineField.FindAllNeighborBombs();
			mineField.RevealTiles(userInput);
			mineField.VerifyGameWon();
			for (auto& tile : mineField.GetField())
				if (tile.GetInformation() == Tile::Information::Bomb)
					Assert::IsTrue(tile.GetState() == Tile::State::Flagged);
		}
		TEST_METHOD(WinOnUpperLineNoCornersSixTilesFree)
		{
			MineField mineField{ 8,4,26 };
			MineField::Position userInput = { 0,4 };
			mineField.SpawnBombs(userInput);
			mineField.FindAllNeighborBombs();
			mineField.RevealTiles(userInput);
			mineField.VerifyGameWon();
			for (auto& tile : mineField.GetField())
				if (tile.GetInformation() == Tile::Information::Bomb)
					Assert::IsTrue(tile.GetState() == Tile::State::Flagged);
		}
		TEST_METHOD(WinOnLowerLineNoCornersSixTilesFree)
		{
			MineField mineField{ 8,4,26 };
			MineField::Position userInput = { 3,4 };
			mineField.SpawnBombs(userInput);
			mineField.FindAllNeighborBombs();
			mineField.RevealTiles(userInput);
			mineField.VerifyGameWon();
			for (auto& tile : mineField.GetField())
				if (tile.GetInformation() == Tile::Information::Bomb)
					Assert::IsTrue(tile.GetState() == Tile::State::Flagged);
		}
		TEST_METHOD(WinOnLeftLineNoCornersSixTilesFree)
		{
			MineField mineField{ 8,4,26 };
			MineField::Position userInput = { 2,0 };
			mineField.SpawnBombs(userInput);
			mineField.FindAllNeighborBombs();
			mineField.RevealTiles(userInput);
			mineField.VerifyGameWon();
			for (auto& tile : mineField.GetField())
				if (tile.GetInformation() == Tile::Information::Bomb)
					Assert::IsTrue(tile.GetState() == Tile::State::Flagged);
		}
		TEST_METHOD(WinOnRightLineNoCornersSixTilesFree)
		{
			MineField mineField{ 8,4,26 };
			MineField::Position userInput = { 2,7 };
			mineField.SpawnBombs(userInput);
			mineField.FindAllNeighborBombs();
			mineField.RevealTiles(userInput);
			mineField.VerifyGameWon();
			for (auto& tile : mineField.GetField())
				if (tile.GetInformation() == Tile::Information::Bomb)
					Assert::IsTrue(tile.GetState() == Tile::State::Flagged);
		}
	};
}
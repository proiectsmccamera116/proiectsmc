# Minesweeper Windows XP in C++
A recreation of the game Minesweeper, released with Windows XP, using modern elements of C++.
## Functionality
* 3 levels of difficulty and a custom one
* ability to restart a game by pressing the face button
* ability to get hints as to press bomb-free tiles
* help button which describes the way to play the game
* ability to left click a tile to place a flag
* a counter for the number of seconds passed since the start of the game
* a counter for the number of bombs left
## Screnshoots
![Scheme](SampleImages/sample1.png)
![Scheme](SampleImages/sample2.png)
![Scheme](SampleImages/sample3.png)
![Scheme](SampleImages/sample4.png)
![Scheme](SampleImages/sample5.png)
![Scheme](SampleImages/sample6.png)
#pragma once

#include <iostream>
#include <string>
#include <chrono>
#include <ctime>
#include <iomanip>

#ifdef LOGGINGDLL_EXPORTS
#define LOGGING_API __declspec(dllexport)
#else
#define LOGGING_API __declspec(dllimport)
#endif

class __declspec(dllexport) Logger
{
public:
	enum class Level : uint8_t
	{
		INFO,
		WARNING,
		ERROR
	};

	static const std::string ConvertLevelToString(Level level);
	template<class ... Args>
	static void Log(std::ostream& outStream, const Level level, Args&& ...params);

private:
	static void LogTime(std::ostream& outStream);
};

template<class ...Args>
inline static void Logger::Log(std::ostream& outStream, Level level, Args&& ...params)
{
	LogTime(outStream);
	outStream << "[" << ConvertLevelToString(level) << "] ";
	((outStream << std::forward<Args>(params) << " "), ...);
	outStream << '\n';
}
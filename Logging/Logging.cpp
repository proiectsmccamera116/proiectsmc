#pragma warning(disable : 4996)

#include "Logging.h"

const std::string Logger::ConvertLevelToString(const Level level)
{
	switch (level)
	{
	case Level::INFO:
		return std::string("INFO");
	case Level::WARNING:
		return std::string("WARNING");
	case Level::ERROR:
		return std::string("ERROR");
	default:
		return "";
	}
}

 void Logger::LogTime(std::ostream& outStream) 
{
	std::time_t crtTime = std::time(nullptr);
	outStream << '[' << std::put_time(std::localtime(&crtTime), "%H:%M:%S") << ']';
}